#!/bin/bash

###########################################################################################################
# Welcome! This script is used to format and test hard disks. It generates a report and sends it by email #
###########################################################################################################

# Check root user
if [[ $UID -ne 0 ]]; then echo -n "User: "; whoami; echo 'Must be root to run this script'; exit 0; fi

# Name of script
SCRIPT=${0:2}

# System disk in the Front-End
SYSTEM_DEVICE=$(lsscsi -t | grep disk | grep -v sas | awk '{print $NF}')

# Block devices in all Disk Array
BLOCK_DEVICES=$(lsscsi -t | grep 'disk.*sas' | awk '{print $NF}' | grep -v '-')

########################################## Can be modified ################################################
###########################################################################################################
MANUFACTURER=Promise

EMAIL="repair.team@cern.ch christian.boissat@cern.ch"
## EMAIL="mickael.ducruet@cern.ch"

REPAIR_FOLDER=/var/log/repair                # Used for operations
TMP_FOLDER=/var/log/repair/tmp               # Used for checks before to launch operations

if [[ ! -d $REPAIR_FOLDER ]]; then mkdir $REPAIR_FOLDER; fi
if [[ ! -d $TMP_FOLDER ]]; then mkdir $TMP_FOLDER; fi

MAGENTA="\e[1;35m"
RED="\e[1;31m"
CYAN="\e[0;36m"
WHITE="\e[97m"
ORANGE="\e[0;33m"
GRAY="\e[0;37m"
GREEN="\e[0;32m"

# Separation characters for the report
SMALL_LINE='-----------------------------------------------------------------------------------------------------------'
BIG_LINE='***********************************************************************************************************'

############################################## Messages ###################################################
###########################################################################################################
function pause() {
    echo; echo -e "$RED"'Please press [ENTER] key to continue'"$GRAY"
    read -r
}

function message_progress() {
    echo; echo -e "$MAGENTA"'Operations are in progress in background...'"$GRAY"
    echo 'You will be notified by email when the work is completed'
    echo
}

function spinner() {
    echo -en "$ORANGE"
    local i=0
    local sp='/-\|'
    local n=${#sp}
    printf ' '
    sleep 0.1
    while [[ -d "/proc/$PID" ]]; do
        printf '\b%s' "${sp:i++%n:1}"
        sleep 0.1
    done
}

############################################### Updates ###################################################
###########################################################################################################
function run_s2cli() {
    # If s2cli is already running, wait the end and do not run it again
    pgrep ^s2cli >/dev/null 2>&1
        if [ $? = 1 ]; then
            s2cli -s >"$TMP_FOLDER/s2cli_${NAME}.txt"
        else
            while pgrep ^s2cli >/dev/null 2>&1; do sleep 1; done
        fi
    
    # Create s2cli files about informations of devices and Disk Array
    grep "$MANUFACTURER" < "$TMP_FOLDER/s2cli_${NAME}.txt" | awk '{print $6}' | while IFS= read -r SN_DA
    do
        < "$TMP_FOLDER/s2cli_${NAME}.txt" grep -B 3 "$MANUFACTURER" | head -n 4 | grep -v '+' > "$TMP_FOLDER/s2cli_title_${SN_DA}_${NAME}.txt"
        < "$TMP_FOLDER/s2cli_${NAME}.txt" grep -A 39 "$SN_DA" | tail -n 26 > "$TMP_FOLDER/s2cli_enclosure_${SN_DA}_${NAME}.txt"
        < "$TMP_FOLDER/s2cli_enclosure_${SN_DA}_${NAME}.txt" grep -v 'UNKNOWN\|NOT PRESENT' | awk '{print $2}'|tr '\n' ' ' > "$TMP_FOLDER/s2cli_full_slot_${SN_DA}_${NAME}.txt"
        < "$TMP_FOLDER/s2cli_enclosure_${SN_DA}_${NAME}.txt" grep 'UNKNOWN' | awk '{print $2}' | tr '\n' ' ' > "$TMP_FOLDER/s2cli_unknown_slot_${SN_DA}_${NAME}.txt"
        < "$TMP_FOLDER/s2cli_enclosure_${SN_DA}_${NAME}.txt" grep 'NOT PRESENT' | awk '{print $2}' | tr '\n' ' ' > "$TMP_FOLDER/s2cli_empty_slot_${SN_DA}_${NAME}.txt"
    done
}

function show_s2cli() {
    echo; echo -en "$MAGENTA"'Displays S.M.A.R.T health and attributes status on attached storage drives'"$ORANGE"
    run_s2cli & PID=$!
    spinner
    echo
    grep "$MANUFACTURER" < "$TMP_FOLDER/s2cli_${NAME}.txt" | awk '{print $6}' | while IFS= read -r SN_DA
    do
        echo; echo; echo -e "$ORANGE"; cat "$TMP_FOLDER/s2cli_title_${SN_DA}_${NAME}.txt"
        echo -e "$WHITE"; cat "$TMP_FOLDER/s2cli_enclosure_${SN_DA}_${NAME}.txt"
        echo; echo -e "$CYAN""Summary of Disk Array $SN_DA"
        echo -en "$WHITE"'Slots FULL   '"$ORANGE"; cat "$TMP_FOLDER/s2cli_full_slot_${SN_DA}_${NAME}.txt"
        echo; echo -en "$WHITE"'Slots UNKNOWN '"$ORANGE"; cat "$TMP_FOLDER/s2cli_unknown_slot_${SN_DA}_${NAME}.txt"
        echo; echo -en "$WHITE"'Slots EMPTY   '"$ORANGE"; cat "$TMP_FOLDER/s2cli_empty_slot_${SN_DA}_${NAME}.txt"  
    done
}

################################################ Checks ###################################################
###########################################################################################################
function check_shred() {
    echo; echo -e "$ORANGE"'Shredding in progress'"$WHITE"
    ps -fp "$(pgrep -d, -x ^shred$)" >/dev/null 2>&1
    if [ $? = 1 ]; then echo -e "$RED"'No current shredding'; return 0; fi
    find "$REPAIR_FOLDER" -type f -name 'operation*shred*' | while IFS= read -r SHRED_PROCESS
    do
        tail -n1 "$SHRED_PROCESS"
    done
    for dev in $BLOCK_DEVICES; do 
        if [ -e $REPAIR_FOLDER/operation*shred*${dev:5}*.log ]; then
            ps -fp "$(pgrep -d, -x ^shred$)" | grep "$dev" >/dev/null 2>&1
            if [ $? = 1 ]; then
                echo; echo -e "$CYAN""$dev$WHITE finished a few moments ago"
            fi
        fi
    done

}

function check_format() {
    echo; echo -e "$ORANGE"'Formatting in progress'"$WHITE"
    ps -fp "$(pgrep -d, -x ^dd$)" 2>/dev/null || echo -e "$RED"'No current formatting'
    for dev in $BLOCK_DEVICES; do 
        if [ -e $REPAIR_FOLDER/operation*format*${dev:5}*.log ]; then
            ps -fp "$(pgrep -d, -x ^dd$)" | grep "$dev" >/dev/null 2>&1
            if [ $? = 1 ]; then
                echo; echo -e "$CYAN""$dev$WHITE finished a few moments ago"
            fi
        fi
    done
}

function check_smart() {
    echo; echo -e "$ORANGE"'S.M.A.R.T. Self-Test in progress'"$WHITE"
    for dev in $BLOCK_DEVICES; do
        if [ -e $REPAIR_FOLDER/operation*smart*${dev:5}*.log ]; then
            echo -e "S.M.A.R.T. on device $dev$WHITE"
            < $REPAIR_FOLDER/operation*smart*${dev:5}*.log grep -i 'Test will complete after\|Estimated completion time'
            smartctl -a $dev | grep 'test remaining' | awk '{print $(NF-3),$(NF-2),$(NF-1),$(NF)}'
            echo
            if [ $? = 1 ]; then
                echo; echo -e "$CYAN""$dev$WHITE finished a few moments ago"
            fi
        fi
    done
    ls $REPAIR_FOLDER/operation*smart* >/dev/null 2>&1 || echo -e "$RED"'No current S.M.A.R.T. Self-test long'
}

function check_date() {
    for d in $(ls "$REPAIR_FOLDER/report_"* 2>/dev/null | grep -v 'sd'); do
        head -n 2 "$d" 2>/dev/null
        echo
    done
}

function check_devices_activities() {
    for dev in $DEVICES; do
        if [ -e $REPAIR_FOLDER/report*${dev:5}*.log ]; then
            echo; echo -e "$MAGENTA""Logs indicate that the $ORANGE$dev$MAGENTA device operations are not yet completed"
            pause; return 1
        fi
    done
}

function check_name() {
    # Check activities by $NAME
    if [ -e "$REPAIR_FOLDER/serials_${NAME}.log" ]; then
        echo; echo -e "$MAGENTA""Logs indicate that the operations of $ORANGE$NAME$MAGENTA are not yet finished"
        pause; return 1
    fi
}

function check_device_available() {
    # Exit if there is no device ready for the operations
    if [ ! -s "$TMP_FOLDER/serials_${NAME}.log" ]; then
        echo; echo -e "$MAGENTA""There are$ORANGE no devices available!$MAGENTA Try to$ORANGE put a new device$MAGENTA or$ORANGE cleans activities"
        pause; return 1
    fi
}

function check_devices_serial() {
    # File used for blockdevices without serial number detected
    touch "$TMP_FOLDER/serials_missing_${NAME}.log"

    # File used for all serial numbers detected
    touch "$TMP_FOLDER/serials_all_${NAME}.log"

    # File used for all serial numbers who have already an activity
    touch "$TMP_FOLDER/serials_other_${NAME}.log"

    # All devices with a serial number "$TMP_FOLDER/serials_all_${NAME}.log"
    echo; echo; echo -en "$MAGENTA""Analyse devices $ORANGE"
    for dev in $BLOCK_DEVICES; do
        if ! sg_vpd --page=sn "$dev" >/dev/null 2>&1; then
            echo -e "$RED""The serial number of $ORANGE$dev$RED cannot be found $WHITE" >>"$TMP_FOLDER/serials_missing_${NAME}.log"
        else
        {
            echo -n "$dev "
            sg_vpd --page=sn "$dev" | grep -i 'Unit serial number:' | awk '{print $NF}'
        } >>"$TMP_FOLDER/serials_all_${NAME}.log"
        fi
    done

    # Other devices "$TMP_FOLDER/serials_other_${NAME}.log"
    for other_serial in "$REPAIR_FOLDER/serials"*; do
        [ -e "$other_serial" ] || break
        cat "$other_serial" >>"$TMP_FOLDER/serials_other_${NAME}.log"
    done

    # Devices ready for operations "$TMP_FOLDER/serials_${NAME}.log"
    if [ -s "$TMP_FOLDER/serials_other_${NAME}.log" ]; then
        sort "$TMP_FOLDER/serials_all_${NAME}.log" "$TMP_FOLDER/serials_other_${NAME}.log" | uniq -u >"$TMP_FOLDER/serials_${NAME}.log"
    else
        cat "$TMP_FOLDER/serials_all_${NAME}.log" >"$TMP_FOLDER/serials_${NAME}.log"
    fi

    # Displays a report of the detection performed
    DEVICES_DETECTED=$(lsscsi -t | grep disk | grep sas | awk '{print $NF}' | wc -l)
    SERIAL_DETECTED=$(< "$TMP_FOLDER/serials_all_${NAME}.log" wc -l)
    DEVICES_OPERATION=$(< "$TMP_FOLDER/serials_other_${NAME}.log" wc -l)
    DEVICES_AVAILABLE=$(echo "$DEVICES_DETECTED-$DEVICES_OPERATION" | bc)
    echo; echo; echo -e "$CYAN"'Summary of the detection'
    echo -e "$WHITE""Devices detected     $ORANGE$DEVICES_DETECTED"
    echo -e "$WHITE""Serials detected     $ORANGE$SERIAL_DETECTED"
    echo -e "$WHITE""Devices in activity  $ORANGE$DEVICES_OPERATION"
    echo -e "$WHITE""Devices available    $ORANGE$DEVICES_AVAILABLE"
}

############################################### Cleaning ##################################################
###########################################################################################################
function clean_tmp() {
    find "$TMP_FOLDER" -type f -delete
}

function clean_all() {
    find "$REPAIR_FOLDER" -type f -delete
    ipmitool sel clear >/dev/null
}

function clean_name() {
    find "$REPAIR_FOLDER" -type f -name "*$NAME*" | while IFS= read -r FILE_NAME
    do
        rm --force "$FILE_NAME" >/dev/null
    done
    find "$REPAIR_FOLDER" -type f -name "SEAGATE*" | while IFS= read -r FILE_NAME
    do
        rm --force "$FILE_NAME" >/dev/null
    done
}

################################################ Killer ###################################################
###########################################################################################################
function kill_all() {
    echo -e "$MAGENTA"
    echo '-------------------'
    echo 'Kill all operations'
    echo -e '-------------------'"$WHITE"
    touch "$REPAIR_FOLDER/smart_lock"

    for gid in $(ps x -o "%p %r %c" | grep "$SCRIPT" | grep -v "$$" | awk '{print $2}' | uniq); do kill -9 -"$gid"; done

    killall -9 dd; killall -9 shred

    for dev in $BLOCK_DEVICES; do smartctl -X $dev >/dev/null 2>&1 & done
    
    clean_all; killall -9 sleep; sleep 5 & PID=$!
    spinner
    rm --force "$REPAIR_FOLDER/smart_lock"
}

function unlock_script() {
    echo -e "$MAGENTA"
    echo '---------------------'
    echo 'Unlock all operations'
    echo '---------------------'
    touch "$REPAIR_FOLDER/smart_lock"

    echo; echo -e "$CYAN"'[ 1 / 10 - KILL DD PID ]'"$WHITE"
    killall -9 dd -w

    echo; echo -e "$CYAN"'[ 2 / 10 - KILL SHRED PID ]'"$WHITE"
    killall -9 shred -w

    echo; echo -e "$CYAN"'[ 3 / 10 - KILL SLEEP PID ]'"$WHITE"
    killall -9 sleep -w

    echo; echo -e "$CYAN"'[ 4 / 10 - KILL S.M.A.R.T. SELF TEST LONG ]'"$WHITE"
    for dev in $BLOCK_DEVICES; do
        if [ -e $REPAIR_FOLDER/operation*smart*${dev:5}*.log ]; then
            smartctl -X $dev >/dev/null 2>&1
        fi
    done
    echo 'Request for aborts launched'

    echo; echo -e "$CYAN"'[ 5 / 10 - KILL SLEEP PID ]'"$WHITE"
    killall -9 sleep -w

    echo; echo -e "$CYAN"'[ 6 / 10 - VERIFICATION OF STILL REMAINING TESTS ]'"$WHITE"
    for dev in $BLOCK_DEVICES; do
        if [ -e $REPAIR_FOLDER/operation*smart*${dev:5}*.log ]; then
            while smartctl -a $dev | grep 'remaining\|progress' >/dev/null 2>&1; do sleep 2; done
        fi
    done
    echo 'OK'

    echo; echo -e "$CYAN"'[ 7 / 10 - KILL SLEEP PID ]'"$WHITE"
    killall -9 sleep -w

    echo; echo -e "$CYAN"'[ 8 / 10 - WAITING FOR THE START OF THE REPORT ]'"$WHITE"
    while ls $REPAIR_FOLDER/operation_*"${NAME}".log >/dev/null 2>&1; do sleep 2; done
    echo 'The report has started'

    echo; echo -e "$CYAN"'[ 9 / 10 - REPORT IN PROGRESS ]'
    echo -e "$ORANGE"'Please wait...'
    while ls $REPAIR_FOLDER/report_* >/dev/null 2>&1; do sleep 2; done
    
    rm --force "$REPAIR_FOLDER/smart_lock"

    echo; echo -e "$CYAN"'[ 10 / 10 - REPORT SENT BY MAIL ]'"$WHITE"
    echo 'DONE'
}

############################################### Choices ###################################################
###########################################################################################################
function choice_name() {
    unset NAME
    while [[ -z "$NAME" ]]; do
    echo; echo -en "$GREEN""What is your name ? $WHITE"
    read -r NAME
    # Delete space in $NAME
    NAME=$(echo "$NAME" | tr -d " ")
        if [ -e "$REPAIR_FOLDER/serials_${NAME}.log" ]; then
            echo -e "$RED""The name $ORANGE${NAME}$RED is already used !"
            unset NAME
        elif
            echo "$NAME" | grep '_' >/dev/null; then
            echo -e "$RED""I do not like the following character: $ORANGE'underscore (_)',$RED please try again !"
        unset NAME
        fi
    done
}

function choice_devices() {
    unset DEVICES
    while [[ -z "$DEVICES" ]]; do
        rm --force "$TMP_FOLDER/serials_${NAME}.log"
        echo; echo -e "$GREEN""Welcome $WHITE${NAME}$GREEN, please select device(s) separated by space:"
        echo -e "$MAGENTA""For example: $ORANGE/dev/sdz /dev/sdaa /dev/sdb $WHITE"
        read -r DEVICES
        echo; echo -en "$MAGENTA""Verification of device(s) $WHITE"
        echo

        # Eliminates possible duplicates
        DEVICES=$(echo "$DEVICES" | tr ' ' '\n' | sort -u)

        # Lock system device
        for dev in $DEVICES; do
            if [[ $dev = "$SYSTEM_DEVICE" ]]; then
                echo -e "$RED""The $ORANGE$dev$RED device is locked because it is the system disk ! $WHITE"
                unset DEVICES
            # Search the serial numbers
            elif ! sg_vpd --page=sn "$dev" >/dev/null 2>&1; then
                echo -e "$RED""The serial number of $ORANGE$dev$RED cannot be found $WHITE"
                unset DEVICES
            elif [[ $DEVICES =~ $dev ]]; then                {
                    echo -n "$dev "
                    sg_vpd --page=sn "$dev" | grep 'Unit serial number:' | awk '{print $NF}'
                } >>"$TMP_FOLDER/serials_${NAME}.log"
                < "$TMP_FOLDER/serials_${NAME}.log" grep "$dev" | tr '\n' ' '
                lsblk -no type,model,size "$dev" 2>/dev/null
            else echo; echo -e "$RED""The block device $ORANGE$dev$RED is not recognized ! $WHITE"
                unset DEVICES
            fi
        done
    done
}

############################################### Reports ###################################################
###########################################################################################################
function report_device_infos() {
    {
        echo ""
        echo ""
        echo "$BIG_LINE"
        echo -n '    DEVICE INFO    |  '
        < "$REPAIR_FOLDER/serials_${NAME}.log" grep "$dev" | tr '\n' ' '
        lsblk -no type,model,size "$dev" 2>/dev/null || echo
        echo "$SMALL_LINE"
    } >>"$REPAIR_FOLDER/report_${dev:5}_${NAME}.log"
}

function report_format() {
    BYTES=$(lsblk -b "$dev" 2>/dev/null | grep disk | awk '{print $4}')
    VALUE=$(< "$REPAIR_FOLDER/format_${dev:5}_${NAME}.log" tail -n 1 | awk '{print $1}')

    # Check empty file
    test -s "$REPAIR_FOLDER/format_${dev:5}_${NAME}.log"
    if [ $? = 1 ]; then
        {
            echo "    FORMATTING     |  The formatting FAILED because the DD PID was killed or never executed"
        } >>"$REPAIR_FOLDER/report_${dev:5}_${NAME}.log"

    # Check null byte copied
    elif < "$REPAIR_FOLDER/format_${dev:5}_${NAME}.log" grep '0 bytes (0 B) copied' >/dev/null; then
        {
            echo "    FORMATTING     |  The formatting FAILED because the disk has no bytes"
        } >>"$REPAIR_FOLDER/report_${dev:5}_${NAME}.log"

    # Check 100%
    elif  [[ "$BYTES" -eq "$VALUE" ]]; then
        {
            echo "    FORMATTING     |  The formatting LOOKS GOOD because it's done at 100% !"
        } >>"$REPAIR_FOLDER/report_${dev:5}_${NAME}.log"

    # All others conditions
    else
        {
            echo "    FORMATTING     |  The formatting FAILED"
        } >>"$REPAIR_FOLDER/report_${dev:5}_${NAME}.log"
        fi

    {
        < "$REPAIR_FOLDER/format_${dev:5}_${NAME}.log" grep -v "No space left on device\|records"
        echo "$SMALL_LINE"
    } >>"$REPAIR_FOLDER/report_${dev:5}_${NAME}.log"
}

function report_shred() {
    RESULT_SHRED=$(< "$REPAIR_FOLDER/shred_${dev:5}_${NAME}.log" grep -ci '100%')

    # Check empty file
    test -s "$REPAIR_FOLDER/shred_${dev:5}_${NAME}.log"
    if [ $? = 1 ]
    then
        {
            echo "    SHREDDING      |  shred was not started because the output file is empty"
        } >>"$REPAIR_FOLDER/report_${dev:5}_${NAME}.log"

    # Check 100%
    elif [[ "$RESULT_SHRED" -eq 2 ]]
    then
        {
            echo "    SHREDDING      |  pass 2/2 (000000)...100%"
            < "$REPAIR_FOLDER/shred_${dev:5}_${NAME}.log" grep -i '100%'
        } >>"$REPAIR_FOLDER/report_${dev:5}_${NAME}.log"

    # Check other possible errors
    else
        {
            echo "    SHREDDING      |  The shredding FAILED"
            tail -n1 "$REPAIR_FOLDER/shred_${dev:5}_${NAME}.log"
        } >>"$REPAIR_FOLDER/report_${dev:5}_${NAME}.log"
    fi
    {
        echo "$SMALL_LINE"
    } >>"$REPAIR_FOLDER/report_${dev:5}_${NAME}.log"
}

function report_smart() {
    {
        echo '    S.M.A.R.T.     |  '
    } >>"$REPAIR_FOLDER/report_${dev:5}_${NAME}.log"

    smartctl -a $dev >"$REPAIR_FOLDER/smart_${dev:5}_${NAME}.log"

    # Copy result of S.M.A.R.T. in the main SMART file
    {
        echo "* * * * * * * S.M.A.R.T. Self-Test Long on $dev * * * * * * *"
        echo "**"
        cat "$REPAIR_FOLDER/smart_${dev:5}_${NAME}.log"
        echo ""
        echo ""
    } >>"$REPAIR_FOLDER/smart_${NAME}.log"

    # Several reports about the last test: aborted, reset or failed; tehn check the value IN_THE_PAST and FAILING_NOW
    {
        < "$REPAIR_FOLDER/smart_${dev:5}_${NAME}.log" grep -B 1 -i "# 1 .*aborted" # Check if the last S.M.A.R.T. Self-Test has been aborted
        < "$REPAIR_FOLDER/smart_${dev:5}_${NAME}.log" grep -B 1 -i "# 1 .*reset" # Check if the last S.M.A.R.T. Self-Test has been reset
        < "$REPAIR_FOLDER/smart_${dev:5}_${NAME}.log" grep -B 1 -i "# 1 .*fail" # Check if the last S.M.A.R.T. Self-Test has been failed
        < "$REPAIR_FOLDER/smart_${dev:5}_${NAME}.log" grep -i 'in_the_past' # Check the value "IN_THE_PAST"
        < "$REPAIR_FOLDER/smart_${dev:5}_${NAME}.log" grep -i 'failing_now' # Check the value "FAILING_NOW"
        echo ""
    } >> "$REPAIR_FOLDER/report_${dev:5}_${NAME}.log"

    # S.M.A.R.T. failed detection
    if < "$REPAIR_FOLDER/smart_${dev:5}_${NAME}.log" grep -i 'not ready\|SMART command failed\|No such device' >>"$REPAIR_FOLDER/report_${dev:5}_${NAME}.log"; then
        echo "" >>"$REPAIR_FOLDER/report_${dev:5}_${NAME}.log"
    fi

    # The S.M.A.R.T. Health is not OK and not passed
    if < "$REPAIR_FOLDER/smart_${dev:5}_${NAME}.log" grep -i "smart.*health" | grep -vi "ok\|passed" >>"$REPAIR_FOLDER/report_${dev:5}_${NAME}.log"; then
        echo "" >>"$REPAIR_FOLDER/report_${dev:5}_${NAME}.log"
    fi

    # Total Uncorrected Errors on SAS device
    UNCORRECTED_READ=$(< "$REPAIR_FOLDER/smart_${dev:5}_${NAME}.log" grep -i 'read:' | awk '{print $NF}')
    UNCORRECTED_WRITE=$(< "$REPAIR_FOLDER/smart_${dev:5}_${NAME}.log" grep -i 'write:' | awk '{print $NF}')
    if [[ -n "$UNCORRECTED_READ" ]]; then
        if [[ "$UNCORRECTED_READ" -ne 0 ]]; then
            {
                echo "Total Uncorrected Reading Errors: $UNCORRECTED_READ"
                echo ""
            } >>"$REPAIR_FOLDER/report_${dev:5}_${NAME}.log"
        fi
    fi
    if [[ -n "$UNCORRECTED_WRITE" ]]; then
        if [[ "$UNCORRECTED_WRITE" -ne 0 ]]; then
            {
                echo "Total Uncorrected Writing Errors: $UNCORRECTED_WRITE"
                echo ""
            } >>"$REPAIR_FOLDER/report_${dev:5}_${NAME}.log"
        fi
    fi

    # Pending Sectors
    PENDING=$(< "$REPAIR_FOLDER/smart_${dev:5}_${NAME}.log" grep -i "current.*pending" | awk '{print $NF}')
    if [ -n "$PENDING" ]; then
        if [[ "$PENDING" -ne 0 ]]; then
            {
                < "$REPAIR_FOLDER/smart_${dev:5}_${NAME}.log" grep -i "current.*pending"
                echo ""
            } >> "$REPAIR_FOLDER/report_${dev:5}_${NAME}.log"
        fi
    fi

    # Offline_Uncorrectable (I had a hard time...)
    UNCORRECTABLE=$(< "$REPAIR_FOLDER/smart_${dev:5}_${NAME}.log" grep -i "Offline_Uncorrectable" | awk '{print $NF}')
    RATIO=$(echo "scale=2; 1/20" | bc)
    BYTES=$(lsblk -b "$dev" 2>/dev/null | grep disk | awk '{print $4}')
    GB=$(echo "$BYTES"/1073741824 | bc 2>/dev/null)
    LIMIT=$(echo "$GB*$RATIO" | bc 2>/dev/null | awk '{print int($1+0.5)}')

    if [[ -n "$UNCORRECTABLE" ]]; then
        if [[ "$UNCORRECTABLE" -gt "$LIMIT" ]]; then
            {
                echo "The tolerance of 1 sector per 20 gigabytes for a $GB GigaByte disk is $LIMIT sectors maximum"
                echo "The number of offline uncorrectable sectors is: $UNCORRECTABLE"
                < "$REPAIR_FOLDER/smart_${dev:5}_${NAME}.log" grep -i "Offline_Uncorrectable"
                echo ""
            } >> "$REPAIR_FOLDER/report_${dev:5}_${NAME}.log"
        fi
    fi
    {
        echo "$BIG_LINE"
    } >>"$REPAIR_FOLDER/report_${dev:5}_${NAME}.log"
}

############################################### Processes #################################################
###########################################################################################################
function create_filename() {
    # Backup all serials in the REPAIR_FOLDER
    echo; echo -e "$ORANGE"'Backup file'"$WHITE"
    < "$TMP_FOLDER/serials_${NAME}.log" sort -u | tee "$REPAIR_FOLDER/serials_${NAME}.log"

    # Backup s2cli
    cp --force $TMP_FOLDER/s2cli* "$REPAIR_FOLDER" 2>/dev/null

    # Create txt files for the model of devices
    touch "$REPAIR_FOLDER/model_SEAGATE_${NAME}.txt"
    touch "$REPAIR_FOLDER/model_TOSHIBA_${NAME}.txt"
    touch "$REPAIR_FOLDER/model_HITACHI_${NAME}.txt"
    touch "$REPAIR_FOLDER/model_WD_${NAME}.txt"

    function seagate () {
        {
            echo -n "$VAR,"
            SEAGATE_FULL_SN=$(< "$REPAIR_FOLDER/serials_${NAME}.log" grep "$dev" | awk '{print $NF}')
            # Take the 8 first caracters of serial number
            echo "${SEAGATE_FULL_SN:0:8}"
        } >>"$REPAIR_FOLDER/model_SEAGATE_${NAME}.txt"
    }

    function toshiba () {
        < "$REPAIR_FOLDER/serials_${NAME}.log" grep "$dev" | awk '{print $NF}' >>"$REPAIR_FOLDER/model_TOSHIBA_${NAME}.txt"
    }

    function hitachi () {
        < "$REPAIR_FOLDER/serials_${NAME}.log" grep "$dev" | awk '{print $NF}' >>"$REPAIR_FOLDER/model_HITACHI_${NAME}.txt"
    }

    function western () {
        < "$REPAIR_FOLDER/serials_${NAME}.log" grep "$dev" | awk '{print $NF}' >>"$REPAIR_FOLDER/model_WD_${NAME}.txt"
    }

    DEVICES=$(< "$REPAIR_FOLDER/serials_${NAME}.log" awk '{print $1}')

    for dev in $DEVICES; do
        VAR=$(lsscsi | grep "$dev" | awk '{print $4}')
        shopt -s nocasematch
        if [[ ${VAR} =~ ^SEAGATE$ ]]; then VAR=$(lsscsi | grep "$dev" | awk '{print $5}'); seagate # Line for $VAR to retrieve the model
        elif [[ ${VAR} =~ ^(ST*) ]]; then seagate

        elif [[ ${VAR} =~ ^TOSHIBA$ ]]; then toshiba # Line for $VAR to retrieve the model (Not used)
        elif [[ ${VAR} =~ ^(MG*) ]]; then toshiba

        elif [[ ${VAR} =~ ^(HITACHI|HGST)$ ]]; then hitachi # Line for $VAR to retrieve the model (Not used)
        elif [[ ${VAR} =~ ^(HU*) ]]; then hitachi

        elif [[ ${VAR} =~ ^(WESTERN|WDC|WD)$ ]]; then western # Line for $VAR to retrieve the model (Not used)
        elif [[ ${VAR} =~ ^(WD*|WU*) ]]; then western
        fi
    done
}

function brain_all() {
    report_device_infos
    begin_format
    sleep 120 # Wait 120 seconds, because after a dd writing error, sometimes the device takes time to return

    # Replace operation to format file to proceed to the next task
    mv --force "$REPAIR_FOLDER/operation_format_${dev:5}_${NAME}.log" "$REPAIR_FOLDER/format_${dev:5}_${NAME}.log" 2>/dev/null

    begin_smart
    sleep 10 # Wait 10 seconds, because on some faulty devices, it can take a long time to start up

    if [ ! -e "$REPAIR_FOLDER/smart_lock" ]; then
        # Wait the end of Self-Test Long for 30 minutes.
        while smartctl -c $dev | grep 'remaining' >/dev/null 2>&1; do sleep 1800; done
        while smartctl -l selftest $dev | grep 'progress' >/dev/null 2>&1; do sleep 1800; done
    fi
    # Delete SMART operation file to proceed to the next task
    rm --force "$REPAIR_FOLDER/operation_smart_${dev:5}_${NAME}.log"
}

function brain_format_only() {
    report_device_infos
    begin_format
    sleep 120 # Wait 120 seconds, because after a dd writing error, sometimes the device takes time to return

    # Replace operation to format file to proceed to the next task
    mv --force "$REPAIR_FOLDER/operation_format_${dev:5}_${NAME}.log" "$REPAIR_FOLDER/format_${dev:5}_${NAME}.log" 2>/dev/null
}

function brain_shred_only() {
    report_device_infos
    begin_shred
    sleep 120 # Wait 120 seconds, because after a dd writing error, sometimes the device takes time to return

    # Replace operation to shred file to proceed to the next task
    mv --force "$REPAIR_FOLDER/operation_shred_${dev:5}_${NAME}.log" "$REPAIR_FOLDER/shred_${dev:5}_${NAME}.log" 2>/dev/null
}

function begin_partition() {
    echo; echo -e "$ORANGE"'Copy 100 input blocks'"$WHITE"
    for dev in $DEVICES; do
        dd of="$dev" if=/dev/zero bs=1M count=100 >"$TMP_FOLDER/partition_${dev:5}_${NAME}.log" 2>&1
        echo -n "$dev => "
        tail -n -1 "$TMP_FOLDER/partition_${dev:5}_${NAME}.log"
    done
}

function begin_format() {
    dd of="$dev" if=/dev/zero bs=1M oflag=direct >"$REPAIR_FOLDER/operation_format_${dev:5}_${NAME}.log" 2>&1
}

function begin_smart() {
    if [ ! -e "$REPAIR_FOLDER/smart_lock" ]; then
        smartctl -t long $dev >"$REPAIR_FOLDER/operation_smart_${dev:5}_${NAME}.log" 2>&1
    fi
}

function begin_shred() {
    shred -fvzn1 "$dev" >"$REPAIR_FOLDER/operation_shred_${dev:5}_${NAME}.log" 2>&1 &
    sleep 10
    while pgrep -a ^shred$ | grep "$dev" >/dev/null; do
        if < "$REPAIR_FOLDER/operation_shred_${dev:5}_${NAME}.log" grep -i 'error'
        then
            PID_SHRED=$(pgrep -a ^shred$ | grep "$dev" | awk '{print $1}')
            kill -9 "$PID_SHRED" >/dev/null 2>&1
        else
            sleep 60
        fi
    done
}

function begin_email() {
    # Wait the end of all operations (5 minutes)
    while ls $REPAIR_FOLDER/operation_*"${NAME}".log >/dev/null 2>&1; do sleep 300; done

    # Create report file
    for dev in $DEVICES; do
        if [ -e "$REPAIR_FOLDER/format_${dev:5}_${NAME}.log" ]; then report_format; fi
        if [ -e "$REPAIR_FOLDER/shred_${dev:5}_${NAME}.log" ]; then report_shred; fi
        report_smart
    done

    # Create the main report file
    for reports in $REPAIR_FOLDER/report_*_"${NAME}".log; do
        [ -e "$reports" ] || break
        {
            cat "$reports"
            echo ""
        } >>"$REPAIR_FOLDER/report_${NAME}.log"
    done

    # Send an email at the repair team
    {
        echo "Dear repair service team,"
        echo ""
        echo "Please find attached the results of the operations concerning the devices below."
        echo ""
    } >"$REPAIR_FOLDER/content_mail_${NAME}.log"

    # For all Disk Array (Use the SN)
    grep "$MANUFACTURER" < "$REPAIR_FOLDER/s2cli_${NAME}.txt" | awk '{print $6}' | while IFS= read -r SN_DA
    do
        {
            echo ""
            echo "** Disk Array with serial number $SN_DA **"
        } >>"$REPAIR_FOLDER/content_mail_${NAME}.log"

        # For all serial numbers in the boucle of each Disk Array
        awk '{print $2}' < "$REPAIR_FOLDER/serials_${NAME}.log" | while IFS= read -r SN_DEVICE
        do
            # If serial number is readable in s2cli, take the SLOT number and the TYPE of device (Disk / UNKNOWN)
            if grep -q "$SN_DEVICE" "$REPAIR_FOLDER/s2cli_enclosure_${SN_DA}_${NAME}.txt"; then
                SLOT_DEVICE=$(< "$REPAIR_FOLDER/s2cli_enclosure_${SN_DA}_${NAME}.txt" grep "$SN_DEVICE" | awk '{print $2}')
                echo "Slot: $SLOT_DEVICE | Serial: $SN_DEVICE" >>"$REPAIR_FOLDER/slot_s2cli_${NAME}.log"

            # Otherwise search the block device associated to find the SLOT number and the TYPE of device (UNKNOWN / Empty)
            else
                SD_DEVICE=$(< "$REPAIR_FOLDER/serials_${NAME}.log" grep "$SN_DEVICE" | awk '{print $1}')
                if grep -q "$SD_DEVICE" "$REPAIR_FOLDER/s2cli_enclosure_${SN_DA}_${NAME}.txt"; then
                    SLOT_DEVICE=$(< "$REPAIR_FOLDER/s2cli_enclosure_${SN_DA}_${NAME}.txt" grep "$SD_DEVICE" | awk '{print $2}')
                    SN_DEVICE=$(< "$REPAIR_FOLDER/serials_${NAME}.log" grep "$SD_DEVICE" | awk '{print $2}')
                    echo "Slot: $SLOT_DEVICE | Serial: $SN_DEVICE" >>"$REPAIR_FOLDER/slot_s2cli_${NAME}.log"
                fi
            fi
        done

        < "$REPAIR_FOLDER/slot_s2cli_${NAME}.log" column -t -s "|" | sort -k2 -g >>"$REPAIR_FOLDER/content_mail_${NAME}.log"

        {
            SLOT_UNKNOWN=$(cat "$REPAIR_FOLDER/s2cli_unknown_slot_${SN_DA}_${NAME}.txt")
            SLOT_EMPTY=$(cat "$REPAIR_FOLDER/s2cli_empty_slot_${SN_DA}_${NAME}.txt")
            echo ""
            echo "Slots UNKNOWN [ $SLOT_UNKNOWN]"
            echo "Slots EMPTY   [ $SLOT_EMPTY]"
            echo ""
            echo ""
            echo ""
        } >>"$REPAIR_FOLDER/content_mail_${NAME}.log"
    done

    # Ready command line
    SERIAL_LIST=$(< "$REPAIR_FOLDER/serials_${NAME}.log" awk '{print $NF}' | awk -F/ '{print $1}' ORS=',' | sed '$ s/.$//')
    
    {
        echo '** Ready command line **'
        echo ".\justBackFROMTbed.ps1 -SN $SERIAL_LIST -DoIt"
        echo ""
        echo ""
        echo ""
    } >>"$REPAIR_FOLDER/content_mail_${NAME}.log"

    # Sorting of disks by manufacturer for extended warranty
    {
        echo '** Sorting of disks by manufacturer for extended warranty **'
        find "$REPAIR_FOLDER" -type f -name "model*${NAME}.txt" | while IFS= read -r FILE_MODEL_DISK
        do
            if [[ -s "$FILE_MODEL_DISK" ]]; then
                MODEL_DISK=$(echo "$FILE_MODEL_DISK: " |  awk -F'[__]' '{print $2}')
                NB_MODEL_DISK=$(grep -cv br "$FILE_MODEL_DISK")
                echo "$MODEL_DISK: $NB_MODEL_DISK"
                cat "$FILE_MODEL_DISK"
                echo
            fi
        done
    } >>"$REPAIR_FOLDER/content_mail_${NAME}.log"

    echo; echo 'Best regards, root' >>"$REPAIR_FOLDER/content_mail_${NAME}.log"

    # Seagate file in .csv and send mail
    if [[ -s "$REPAIR_FOLDER/model_SEAGATE_${NAME}.txt" ]]; then
    NB_LINE=$(sed -n '$=' "$REPAIR_FOLDER/model_SEAGATE_${NAME}.txt")
        if [[ $NB_LINE -le 10 ]]; then
            cp "$REPAIR_FOLDER/model_SEAGATE_${NAME}.txt" "$REPAIR_FOLDER/SEAGATE_1.csv"
            < "$REPAIR_FOLDER/content_mail_${NAME}.log" mail -s "Results of ${NAME}@$HOSTNAME operations" -a "$REPAIR_FOLDER/report_${NAME}.log" -a "$REPAIR_FOLDER/smart_${NAME}.log" -a "$REPAIR_FOLDER/SEAGATE_1.csv" $EMAIL
        elif [[ $NB_LINE -le 20 ]]; then
            sed -n '1,10p' "$REPAIR_FOLDER/model_SEAGATE_${NAME}.txt" >"$REPAIR_FOLDER/SEAGATE_1.csv"
            sed -n '11,20p' "$REPAIR_FOLDER/model_SEAGATE_${NAME}.txt" >"$REPAIR_FOLDER/SEAGATE_2.csv"
            < "$REPAIR_FOLDER/content_mail_${NAME}.log" mail -s "Results of ${NAME}@$HOSTNAME operations" -a "$REPAIR_FOLDER/report_${NAME}.log" -a "$REPAIR_FOLDER/smart_${NAME}.log" -a "$REPAIR_FOLDER/SEAGATE_1.csv" -a "$REPAIR_FOLDER/SEAGATE_2.csv" $EMAIL

        elif [[ $NB_LINE -le 30 ]]; then
            sed -n '1,10p' "$REPAIR_FOLDER/model_SEAGATE_${NAME}.txt" >"$REPAIR_FOLDER/SEAGATE_1.csv"
            sed -n '11,20p' "$REPAIR_FOLDER/model_SEAGATE_${NAME}.txt" >"$REPAIR_FOLDER/SEAGATE_2.csv"
            sed -n '21,30p' "$REPAIR_FOLDER/model_SEAGATE_${NAME}.txt" >"$REPAIR_FOLDER/SEAGATE_3.csv"
            < "$REPAIR_FOLDER/content_mail_${NAME}.log" mail -s "Results of ${NAME}@$HOSTNAME operations" -a "$REPAIR_FOLDER/report_${NAME}.log" -a "$REPAIR_FOLDER/smart_${NAME}.log" -a "$REPAIR_FOLDER/SEAGATE_1.csv" -a "$REPAIR_FOLDER/SEAGATE_2.csv" -a "$REPAIR_FOLDER/SEAGATE_3.csv" $EMAIL
        fi
    else
        < "$REPAIR_FOLDER/content_mail_${NAME}.log" mail -s "Results of ${NAME}@$HOSTNAME operations" -a "$REPAIR_FOLDER/report_${NAME}.log" -a "$REPAIR_FOLDER/smart_${NAME}.log" $EMAIL
    fi
    clean_name
}

############################################### Header menu ###############################################
###########################################################################################################
function menu() {
    clear
    echo; echo -en "$MAGENTA"
    uptime
    echo
    echo -e "$GRAY""Hostname      | $WHITE$HOSTNAME"
    echo -e "$GRAY""Serial number | $WHITE$(dmidecode -s system-serial-number)"
    echo -e "$GRAY""Script name   | $WHITE$SCRIPT"
    echo -e "$GRAY""Folders used  | $WHITE$REPAIR_FOLDER"
    echo -e "$GRAY""System disk   | $WHITE$SYSTEM_DEVICE"
    echo -e "$GRAY""Disk Array    | $WHITE$MANUFACTURER"
    echo -e "$GRAY""Email address | $WHITE$EMAIL"
    echo -e "$RED"
    echo " ╦═╗┌─┐┌─┐┌─┐┬┬─┐  ┌─┐┌─┐┬─┐┬  ┬┬┌─┐┌─┐  ┌┬┐┌─┐┌─┐┬   "
    echo " ╠╦╝├┤ ├─┘├─┤│├┬┘  └─┐├┤ ├┬┘└┐┌┘││  ├┤    │ │ ││ ││   "
    echo " ╩╚═└─┘┴  ┴ ┴┴┴└─  └─┘└─┘┴└─ └┘ ┴└─┘└─┘   ┴ └─┘└─┘┴─┘ "
    echo -e "                                                 v6.4 ""$ORANGE"
}

############################################### Main menu #################################################
###########################################################################################################
while :; do
    menu
    echo ' 0. Formatting and testing of disks'
    echo
    echo ' 1. Formatting of disks'
    echo
    echo ' 2. Securely delete files'
    echo
    echo ' 3. To see the disks and tasks in progress'
    echo
    echo ' 4. Cancel all tasks or unblock the script'
    echo
    echo '99. Exit'
    echo; echo -e "\e[1;36;44m[✔] Checking operations"
    echo
    check_date 2>/dev/null
    echo -e "\e[0m"
    echo -en "$GREEN""What is your choice ? $ORANGE [0/1/2/3/4/99] $GRAY"
    read -r MAIN_MENU
    case $MAIN_MENU in

    0) # Formatting and testing of disks
        while :; do
            menu
            echo '00. Performs all operations on all devices'
            echo
            echo '01. Performs all operations on selected device(s)'
            echo
            echo '99. Back to the main menu'
            echo
            echo -en "$GREEN""What is your choice ? $ORANGE [00/01/99] $GRAY"
            read -r CHOICE_0
            case $CHOICE_0 in

            00) # Performs all operations on all devices
                clean_tmp
                NAME=root
                check_name; if [ $? = 1 ]; then break; fi
                show_s2cli
                check_devices_serial & PID=$!
                spinner
                echo
                check_device_available; if [ $? = 1 ]; then break; fi

                echo; echo -e "$MAGENTA""You have selected $ORANGE\"00. Performs all operations on all devices\""
                echo; echo -en "$GREEN""Do you want to continue ?$WHITE [y/n] "

                while :; do
                    read -r CHOICE_00
                    case $CHOICE_00 in
                    [y/Y])
                        create_filename
                        begin_partition
                        {
                            echo "Performs all operations on all devices by ${NAME}"
                            date
                            echo ""
                            echo "For the device to be considered good:"
                            echo "In the formatting part must be written 'LOOKS GOOD'"
                            echo "In the S.M.A.R.T. part must be 'BLANK'"
                            echo ""
                        } >> "$REPAIR_FOLDER/report_${NAME}.log"

                        echo; echo -en "$MAGENTA""Formatting starts,$ORANGE please wait..."
                        for dev in $DEVICES; do brain_all & done
                        sleep 5 & PID=$!
                        spinner
                        echo
                        check_format
                        begin_email & message_progress
                        echo -e "$GRAY"
                        exit 0
                        ;;

                    [n/N])
                        break
                        ;;

                    *) echo; echo -en "$GREEN""Do you want to continue ?$WHITE [y/n] "
                        ;;

                    esac
                done
                ;;

            01) # Performs all operations on selected device(s)
                clean_tmp
                choice_name
                check_name; if [ $? = 1 ]; then break; fi
                show_s2cli
                check_devices_serial & PID=$!
                spinner
                echo
                check_device_available; if [ $? = 1 ]; then break; fi
                choice_devices
                check_devices_activities; if [ $? = 1 ]; then break; fi

                echo; echo -e "$MAGENTA""You have selected $ORANGE\"01. Performs all operations on selected device(s)\""
                echo; echo -en "$GREEN""Do you want to continue ?$WHITE [y/n] "

                while :; do
                    read -r CHOICE_01
                    case $CHOICE_01 in
                    [y/Y])
                        create_filename
                        begin_partition
                        {
                            echo "Performs all operations on selected device(s) by ${NAME}"
                            date
                            echo ""
                            echo "For the device to be considered good:"
                            echo "In the formatting part must be written 'LOOKS GOOD'"
                            echo "In the S.M.A.R.T. part must be 'BLANK'"
                            echo ""
                        } >> "$REPAIR_FOLDER/report_${NAME}.log"

                        echo; echo -en "$MAGENTA""Formatting starts,$ORANGE please wait..."
                        for dev in $DEVICES; do brain_all & done
                        sleep 5 & PID=$!
                        spinner
                        echo
                        check_format
                        begin_email & message_progress
                        echo -e "$GRAY"
                        exit 0
                        ;;

                    [n/N])
                        break
                        ;;

                    *) echo -en "$GREEN""Do you want to continue ?$WHITE [y/n] "
                        ;;

                    esac
                done
                ;;

            99) # Back to the main menu
                break
                ;;

            *) ;;

            esac
        done
        ;;

    1) # Formatting of disks
        while :; do
            menu
            echo '10. Starts formatting on all devices'
            echo
            echo '11. Starts formatting on selected device(s)'
            echo
            echo '99. Back to the main menu'
            echo
            echo -en "$GREEN""What is your choice ? $ORANGE [10/11/99] $GRAY"
            read -r CHOICE_1
            case $CHOICE_1 in

            10) # Starts formatting on all devices
                clean_tmp
                NAME=root
                check_name; if [ $? = 1 ]; then break; fi
                show_s2cli
                check_devices_serial & PID=$!
                spinner
                echo
                check_device_available; if [ $? = 1 ]; then break; fi

                echo; echo -e "$MAGENTA""You have selected $ORANGE\"10. Starts formatting on all devices\""
                echo; echo -en "$GREEN""Do you want to continue ?$WHITE [y/n] "

                while :; do
                    read -r CHOICE_10
                    case $CHOICE_10 in
                    [y/Y])
                        create_filename
                        begin_partition
                        {
                            echo "Performs formatting on all devices by ${NAME}"
                            date
                            echo ""
                            echo "For the device to be considered formatted:"
                            echo "In the formatting part must be written 'LOOKS GOOD'"
                            echo ""
                        } >> "$REPAIR_FOLDER/report_${NAME}.log"

                        echo; echo -en "$MAGENTA""Formatting starts,$ORANGE please wait..."
                        for dev in $DEVICES; do brain_format_only & done
                        sleep 5 & PID=$!
                        spinner
                        echo
                        check_format
                        begin_email & message_progress
                        echo -e "$GRAY"
                        exit 0
                        ;;

                    [n/N])
                        break
                        ;;

                    *) echo -en "$GREEN""Do you want to continue ?$WHITE [y/n] "
                        ;;

                    esac
                done
                ;;

            11) # Starts formatting on selected device(s)
                clean_tmp
                choice_name
                check_name; if [ $? = 1 ]; then break; fi
                show_s2cli
                check_devices_serial & PID=$!
                spinner
                echo
                check_device_available; if [ $? = 1 ]; then break; fi
                choice_devices
                check_devices_activities; if [ $? = 1 ]; then break; fi

                echo; echo -e "$MAGENTA""You have selected $ORANGE\"11. Starts formatting on selected device(s)\""
                echo; echo -en "$GREEN""Do you want to continue ?$WHITE [y/n] "

                while :; do
                    read -r CHOICE_11
                    case $CHOICE_11 in
                    [y/Y])
                        create_filename
                        begin_partition
                        {
                            echo "Performs formatting on selected device(s) by ${NAME}"
                            date
                            echo ""
                            echo "For the device to be considered formatted:"
                            echo "In the formatting part must be written 'LOOKS GOOD'"
                            echo ""
                        } >> "$REPAIR_FOLDER/report_${NAME}.log"

                        echo; echo -en "$MAGENTA""Formatting starts,$ORANGE please wait..."
                        for dev in $DEVICES; do brain_format_only & done
                        sleep 5 & PID=$!
                        spinner
                        echo
                        check_format
                        begin_email & message_progress
                        echo -e "$GRAY"
                        exit 0
                        ;;

                    [n/N])
                        break
                        ;;

                    *) echo -en "$GREEN""Do you want to continue ?$WHITE [y/n] "
                        ;;

                    esac
                done
                ;;

            99) # Back to the main menu
                break
                ;;

            *) ;;

            esac

        done
        ;;

            2) # Securely delete files
                clean_tmp
                choice_name
                check_name; if [ $? = 1 ]; then break; fi
                show_s2cli
                check_devices_serial & PID=$!
                spinner
                echo
                check_device_available; if [ $? = 1 ]; then break; fi
                choice_devices
                check_devices_activities; if [ $? = 1 ]; then break; fi

                echo; echo -e "$MAGENTA""You have selected $ORANGE\"2. Securely delete files\"$RED"
                echo 'Deletes the contents of the disk without the possibility of recovery'
                echo; echo -en "$GREEN""Do you want to continue ?$WHITE [y/n] "

                while :; do
                    read -r CHOICE_2
                    case $CHOICE_2 in
                    [y/Y])
                        create_filename
                        begin_partition
                        {
                            echo "Performs shreddring on selected device(s) by ${NAME}"
                            date
                            echo ""
                            echo "For the device to be considered shredded:"
                            echo "In the SHREDDING section, 'pass 2/2 (000000)...100%' must be written"
                            echo ""
                        } >> "$REPAIR_FOLDER/report_${NAME}.log"

                        echo; echo -en "$MAGENTA""Shredding starts,$ORANGE please wait..."
                        for dev in $DEVICES; do brain_shred_only & done
                        sleep 5 & PID=$!
                        spinner
                        echo
                        check_shred
                        begin_email & message_progress
                        echo -e "$GRAY"
                        exit 0
                        ;;

                    [n/N])
                        break
                        ;;

                    *) echo -en "$GREEN""Do you want to continue ?$WHITE [y/n] "
                        ;;

                    esac
                done
                ;;

    3) # To see the disks and tasks in progress
        clean_tmp
        NAME=check
        echo; echo -en "$GREEN""Do you need the table generated by s2cli ?$WHITE [y/n] "

        while :; do
            read -r CHOICE_30
            case $CHOICE_30 in
            [y/Y])
                show_s2cli
                break
                ;;

            [n/N])
                break
                ;;

            *) echo -en "$GREEN""Do you need the table generated by s2cli ?$WHITE [y/n] "
                ;;

            esac
        done
        check_devices_serial & PID=$!
        spinner
        echo
        check_format; check_smart; check_shred
        echo; echo -e "$MAGENTA""The script will return to the$ORANGE main menu"
        pause
        ;;

    4) # Cancel all tasks or unblock the script
        while :; do
            menu
            echo '40. Automatic script unlocking'
            echo
            echo '41. Kill all activities'
            echo
            echo '99. Back to the main menu'
            echo
            echo -en "$GREEN""What is your choice ? $ORANGE [40/41/99] $GRAY"
            read -r CHOICE_4
            case $CHOICE_4 in

            40) # Automatic script unlocking
                check_format; check_smart; check_shred
                echo; echo -e "$MAGENTA""You have selected $ORANGE\"40. Automatic script unlocking\"$GRAY"
                echo 'All remaining activities will be UNLOCKED, report sent immediatly after that'
                echo; echo -en "$GREEN""Do you want to continue ?$WHITE [y/n] "

                while :; do
                    read -r CHOICE_40
                    case $CHOICE_40 in
                    [y/Y])
                        clear
                        unlock_script
                        echo; echo -e "$MAGENTA"
                        echo '---------------------'
                        echo 'Result all operations'
                        echo '---------------------'
                        check_format; check_smart; check_shred
                        echo; echo -e "$MAGENTA"'All tasks are completed'"$GRAY"
                        echo 'You should have received an email including the report'
                        echo
                        pause; break
                        ;;

                    [n/N])
                        break
                        ;;

                    *) echo -en "$GREEN""Do you want to continue ?$WHITE [y/n] "
                        ;;

                    esac
                done
                ;;

            41) # Kill all activities
                check_format; check_smart; check_shred
                echo; echo -e "$MAGENTA""You have selected $ORANGE\"41. Kill all activities\"$GRAY"
                echo 'All remaining activities will be CANCELLED, no report sent'
                echo; echo -en "$GREEN""Do you want to continue ?$WHITE [y/n] "

                while :; do
                    read -r CHOICE_41
                    case $CHOICE_41 in
                    [y/Y])
                        kill_all
                        echo; echo -e "$MAGENTA"
                        echo '---------------------'
                        echo 'Result all operations'
                        echo '---------------------'
                        clean_all; check_format; check_smart; check_shred
                        echo; echo -e "$MAGENTA"'All tasks are completed'
                        pause; break
                        ;;

                    [n/N])
                        break
                        ;;

                    *) echo -en "$GREEN""Do you want to continue ?$WHITE [y/n] "
                        ;;

                    esac
                done
                ;;

            99) # Back to the main menu
                break
                ;;

            *) ;;

            esac
        done
        ;;

    99) # Exit
        echo -e "$RED"'Goodbye ! '"$GRAY"
        exit 0
        ;;

    *) ;;

    esac
done
